import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  satisfactionLevel: number = 0;
  paymentSatisfactionLevel: number = 0;
  CSSatisfactionLevel: number = 0;
  comments: string = '';
  submittingFeedBack: boolean = false;
  submitted: boolean = false;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }
  setSatisfactionLevel(level: number) {
    if (this.satisfactionLevel == level) {
      this.satisfactionLevel = 0;
      return;
    }
    this.satisfactionLevel = level;
  }
  setPaymentSatisfactionLevel(level: number) {
    if (this.paymentSatisfactionLevel == level) {
      this.paymentSatisfactionLevel = 0;
      return;
    }
    this.paymentSatisfactionLevel = level;
  }
  setCSSatisfactionLevel(level: number) {
    if (this.CSSatisfactionLevel == level) {
      this.CSSatisfactionLevel = 0;
      return;
    }
    this.CSSatisfactionLevel = level;
  }
  sendFeedback() {
    if (!this.satisfactionLevel && !this.paymentSatisfactionLevel && !this.CSSatisfactionLevel) {
      return;
    }
    this.submittingFeedBack = true;

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    })

    let body = {
      satisfaction: this.satisfactionLevel,
      customerService: this.CSSatisfactionLevel,
      payment: this.paymentSatisfactionLevel,
      comments: this.comments
    }

    let handelSuccessResponse = (res: any) => {
      if (res.info.status == 200) {
        this.submitted = true;
        this.satisfactionLevel = 0;
        this.CSSatisfactionLevel = 0;
        this.paymentSatisfactionLevel = 0;
        this.comments = '';
      }
    }

    let errorResponse = (res: any) => {
      this.submittingFeedBack = false;
      alert('Sorry! Someting went wrong could not save your feedback')
    }
    let complete = () => {
      this.submittingFeedBack = false;
    }
    this.http.post('https://solavievefeedbackpage.herokuapp.com/feedback/save', body, {
      headers: headers
    }).subscribe({
      next: handelSuccessResponse,
      complete: complete,
      error: errorResponse,
    })
  }


}
